(require '[cljs.build.api :refer build])

(build "src"
    {:main 'lumo-sample
     :output-to "out/main.js"
     :optimizations :advanced
     :target :nodejs})