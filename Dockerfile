FROM anmonteiro/lumo:1.7.0

WORKDIR /usr/src/app

COPY package.json .

RUN npm install -g nodemon
RUN npm install --quiet

COPY . .

RUN npm build

ENTRYPOINT [ "nodemon" ]
